class CreateFirstNames < ActiveRecord::Migration
  def change
    create_table :first_names do |t|
      t.string :lastName
      t.integer :birthday
      t.string :website

      t.timestamps
    end
  end
end
